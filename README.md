# QDataViewer Playground

## Usage 

This example application was built using Kevin's widget:

Installation:
```bash
pip install -e .
```

Run example applciation:
```python
qdataviewer-playground
```

## Steps needed for integration

In general the qdataviewer looks nice and I would like not like to change
its general functionality (except to integrate it in accwidgets).

### Discovered Limitations:
- The Editing Button Bar is not extensible
- Undo / Redo / Send are not yet implemented, Especially Undo and Redo will
  be quite a bit of work, since for them we will have to keep a change history
- Functionalities are not seperated from each other, I feel like it would be
  nicer to have these different functionalities clearly seperated from each
  other (f.e. the slot an editing button is connected to should not directly
  access the plot, but rather do a transformation of the data it is passed)

### Discovered Bugs:
- Selection buggy with multiple plots:
  - select points in graph 1
  - select points in graph 2 with different region than the first graph
  - Move points in graph 1 -> instead of the actual selected region, the points
    in the region of the second selection are moved, but on the first graph

### Dividing the work into smalled Steps:

Point 1 & 2 are roughly equivalent to Kevin's state, all others are additional
work to make the editable charts more useful.

1. Curve & ViewBox with the custom MouseEvents; 
   DataSource preparations; 
   Button Bar Data with Enable, Reset & Send Method **(4d)**
2. Button Bar Functions for extending the Button Bar **(2d)**
3. Add support for Multi Curve Editing **(2d)**
4. Add support for multi layer editing **(3d)**
5. Add Undo / Redo Functionality **(5d)**


## Modular Editing Toolbar Widget

### Functionality

Standard Functions:
- Enable / Disable: Editing Mode
- Undo / Redo: stepwise rollback and repeat
- Reset: Throw away all changes
- Send: Persist modifications on device

All other functions should be modular. We can provide the same functions as
Kevin has added + own ones defined by the application developer.

- The Toolbar is a separate widget, that can be connected to different
  editable PlotWidgets
- The Bar can be extended by adding new buttons
  - Abstract baseclass providing a button + transform function
  - transform function([x], [y]) -> [x], [y] -> Does not access the plot
    directly
  - If the button is clicked, the toolbar gets the selected data, calls the
    transformation with it and send the data back to the Plot

### Signals:
- sig_edit_mode_enable(bool)
  Disables / Enable Editing Mode for each connected Plot
- reset: Reset all Plots by refetching the data from the Update Source
- send: Send the new states of each plot to the attached device

### Slots:
- point_selection():
  Informs the bar about point selections from a Plot Widget, the new selected
  data can be fetched through the get_selection function of the Plot Widget
- points_unselected()
  Informs the bar that points in a widget were unselected
- plot_state_changed()
  Informs the bar about changes in the plot's state

### Functions:
- undo(): Call the undo function of the most recently changed plot
- redo(): call the redo function of the next changed plot

### Others:
- Saves a list of plots that were changed recently, which allows rolling back
  to the last saved state


## Changes on the PlotItem / PlotWidget / ViewBox Level:

### Additional Slots on the PlotItem / PlotWidget:

- edit_mode(enable: bool) -> None:
  Switch between view and editing mode + call edit_unselect()
- edit_unselect() -> None:
  Unselect all prior taken selections, can be called by the Toolbar, if a
  selection happened in another plot -> no multiple selections should be allowed
  at the same time
- edit_replace_selection(Tuple[np.ndarray, np.ndarray]) -> None:
  Replace the current selection with the given one
- edit_reset()
  Throw away all modifications (simply reload data from the data source)
- edit_send()
  Send all modifications for all curves to the attached device
- edit_undo()
  Undo the last state change for the last edited curve
- edit_redo()
  Redo the most recent state change for the next edited curve

### Additional Signals on the PlotItem / PlotWidget:

- sig_selection_changed:
  Notifies about a new selection of points

### Additional Functions:

- get_selection() -> [x], [y]:
  Get the current selection of this Plot -> We do not send the selected data
  but offer a function to fetch it (f.e. if the slected data changed after the
  signal was sent)

### Other

- History of which curve was changed most recently -> When undoing on the entire
  plot, the curve's undo function can be called -> Accurate undo
- Selection of which curve is supposed to be edited (Maybe combobox in the plot)
  Depending on what curve is selected there, the slots are forwared to the
  different PlotDataItems
- Multiple layers can be still supported in the following way:
  -> Forward Mouse Event from the ViewBox to the PlotItem, which looks up the 
     vb in which the item is which should be edited and forwards the event to
     it


## Changes on the PlotDataItem / Curve Level

### Signals
- points_changed:
  In case points in the curve were moved, the Plot has to be notified, so it can
  save the state change for undo's and redo's in its history (only reference
  for the curve, not the curve state)

### Slots
- send()
  Send the current state to the device
- reset()
  Fetch data from the Data Source and replace the currently shown with that
  (instead of undoing the entire history, refetching the data makes this
  operation undoable)
- undo()
  Jump back to the last saved state of the curve
- redo()
  Jump forward to the next saved state of the curve

### Other
- Custom Mouse Event (Kevins implementation can be take for that) for moving
  points up and down (ScatterPlotItem) with the mouse
- Extend the Update Source to allow signals for not only updating from the
  source but also back to where it is connected to
- Each state change should be persisted after the change for the undo / redo 
  operation. Instead of the diff between the states, save the entire state.


