from functools import partial

import numpy as np
import pyqtgraph as pg
import qtawesome as qta
from qtpy import QtCore, QtGui, QtWidgets
from scipy.optimize import curve_fit
from scipy.signal import savgol_filter


class QDataViewer(QtWidgets.QWidget):

    def __init__(self, parent=None, rows=1, cols=1, titles=[]) -> None:
        super().__init__(parent)

        self.n_rows = rows
        self.n_cols = cols
        self.titles = titles

        graph_widget = self.create_graph_widget()
        button_bar = self.create_and_connect_buttons()

        self.setLayout(QtWidgets.QVBoxLayout())
        self.layout().addWidget(graph_widget)
        self.layout().addWidget(button_bar)

        pg.setConfigOption('foreground', QtGui.QColor(192, 192, 192))

    def create_graph_widget(self) -> pg.GraphicsLayoutWidget:
        graph_widget: pg.GraphicsLayoutWidget = pg.GraphicsLayoutWidget()
        graph_widget.setBackground(QtGui.QColor(24, 24, 24))
        graph_widget.clear()

        self.axes = []
        for m in range(self.n_cols * self.n_rows):
            try:
                self.axes.append(pg.PlotItem(self.titles[m]))
            except IndexError:
                self.axes.append(pg.PlotItem())

        # graph_widget.addItem(self.axes[0], 0, 0, 1, 1)
        # graph_widget.addItem(self.axes[1], 1, 0, 1, 1)
        # graph_widget.addItem(self.axes[2], 2, 0, 1, 1)
        for i in range(self.n_rows):
            for j in range(self.n_cols):
                graph_widget.addItem(self.axes[i], i, j, 1, 1)

        # Build references to the different plot items
        # ============================================
        cols = ['darkgoldenrod', 'firebrick', 'forestgreen', 'navy']
        self.lines = [ax.plot([0],
                              [0],
                              pen=pg.mkPen(color=QtGui.QColor(cols[i])),
                              width=2,
                              symbol='o',
                              symbolSize=8,
                              symbolBrush=pg.mkBrush(color=QtGui.QColor(cols[i]))
                              ) for i, ax in enumerate(self.axes)]

        self.editingFinished = {ax: True for ax in self.axes}
        self.originalMouseDragEvent = {ax: ax.getViewBox().mouseDragEvent for ax in self.axes}

        return graph_widget

    def style_axes(self, axes):

        locs = ['top', 'left', 'right', 'bottom']
        for ax in axes:
            for loc in locs:
                ax.showAxis(loc)
            #         ax.getAxis(loc).setPen(pg.mkPen(color=QtGui.QColor('0.2'), width=2))
            ax.getAxis('top').setTicks([])
            ax.getAxis('right').setTicks([])
            ax.showGrid(True, True, alpha=0.2)

    def create_and_connect_buttons(self):

        buttonBar = QtWidgets.QWidget()
        buttonBar.setLayout(QtWidgets.QHBoxLayout())

        self.pbEdit: QtWidgets.QPushButton = QtWidgets.QPushButton()
        self.pbEdit.setIcon(qta.icon("mdi.chart-line"))
        self.pbEdit.setIconSize(QtCore.QSize(24, 24))
        self.pbEdit.setCheckable(True)
        self.pbEdit.toggled.connect(lambda x: self.switch_edit_mode(x))
        self.pbEdit.setChecked(False)
        buttonBar.layout().addWidget(self.pbEdit)

        self.pbAlign: QtWidgets.QPushButton = QtWidgets.QPushButton()
        self.pbAlign.setIcon(qta.icon("mdi.ray-start-end"))
        self.pbAlign.setIconSize(QtCore.QSize(24, 24))
        self.pbAlign.clicked.connect(self.align_points)
        buttonBar.layout().addWidget(self.pbAlign)

        self.pbLine: QtWidgets.QPushButton = QtWidgets.QPushButton()
        self.pbLine.setIcon(qta.icon("mdi.vector-line"))
        self.pbLine.setIconSize(QtCore.QSize(24, 24))
        self.pbLine.clicked.connect(self.line_fit)
        buttonBar.layout().addWidget(self.pbLine)

        self.pbSmooth: QtWidgets.QPushButton = QtWidgets.QPushButton()
        self.pbSmooth.setIcon(qta.icon("mdi.vector-polyline"))
        self.pbSmooth.setIconSize(QtCore.QSize(24, 24))
        self.pbSmooth.clicked.connect(self.poly_fit)
        buttonBar.layout().addWidget(self.pbSmooth)

        self.pbFilter: QtWidgets.QPushButton = QtWidgets.QPushButton()
        self.pbFilter.setIcon(qta.icon("mdi.filter"))
        self.pbFilter.setIconSize(QtCore.QSize(24, 24))
        self.pbFilter.clicked.connect(self.smooth_points)
        buttonBar.layout().addWidget(self.pbFilter)

        self.pbBack: QtWidgets.QPushButton = QtWidgets.QPushButton()
        self.pbBack.setIcon(qta.icon("mdi.arrow-left"))
        self.pbBack.setIconSize(QtCore.QSize(24, 24))
        # self.pbBack.clicked.connect()
        buttonBar.layout().addWidget(self.pbBack)

        self.pbForeward: QtWidgets.QPushButton = QtWidgets.QPushButton()
        self.pbForeward.setIcon(qta.icon("mdi.arrow-right"))
        self.pbForeward.setIconSize(QtCore.QSize(24, 24))
        # self.pbForeward.clicked.connect()
        buttonBar.layout().addWidget(self.pbForeward)

        self.pbSend: QtWidgets.QPushButton = QtWidgets.QPushButton()
        self.pbSend.setIcon(qta.icon("mdi.send"))
        self.pbSend.setIconSize(QtCore.QSize(24, 24))
        # self.pbSend.clicked.connect()
        buttonBar.layout().addWidget(self.pbSend)

        return buttonBar

    def get_settings(self):
        pass

    def set_settings(self):
        pass

    def switch_edit_mode(self, edit_mode):
        self.edit_mode = edit_mode

        if self.edit_mode:
            for ax in self.axes:
                ax.getViewBox().mouseDragEvent = partial(self.customMouseDragEvent, axes=ax)
                ax.listDataItems()[0].scatter.mouseDragEvent = partial(self.scatterMouseDragEvent, axes=ax)
        else:
            for ax in self.axes:
                ax.getViewBox().mouseDragEvent = self.originalMouseDragEvent[ax]
                ax.listDataItems()[0].scatter.mouseDragEvent = self.emptyEvent

    def finishEdit(self):
        for ax in self.axes:
            lineData = ax.listDataItems()[0]
            data = np.array(lineData.scatter.getData())
            lineData.setData(data[0, :], data[1, :])
            self.editingFinished[ax] = True

    # PUSHBUTTON FUNCTIONS
    # ====================
    def align_points(self):

        spinBox = pg.SpinBox()
        dialog = QtWidgets.QDialog()
        dialog.setWindowIconText("Align value")
        dialog.setLayout(QtWidgets.QVBoxLayout())
        buttonBar = QtWidgets.QDialogButtonBox(QtWidgets.QDialogButtonBox.Ok | QtWidgets.QDialogButtonBox.Cancel)

        dialog.layout().addWidget(spinBox)
        dialog.layout().addWidget(buttonBar)

        def accept():
            for ax in self.axes:
                if not self.editingFinished[ax]:
                    lineData = ax.listDataItems()[0]
                    data = np.array(lineData.scatter.getData())
                    data[1, self.selected_indices] = spinBox.value()
                    lineData.setData(data[0, :], data[1, :])

                    self.editingFinished[ax] = True

            dialog.close()

        def reject():
            dialog.close()

        buttonBar.accepted.connect(accept)
        buttonBar.rejected.connect(reject)

        dialog.exec_()

    def line_fit(self):

        def func(x, m, c):
            return m * x + c

        for ax in self.axes:
            if not self.editingFinished[ax]:
                lineData = ax.listDataItems()[0]
                data = np.array(lineData.scatter.getData())
                popt, pcov = curve_fit(func, data[0, self.selected_indices], data[1, self.selected_indices])
                print(popt)
                data[1, self.selected_indices] = func(data[0, self.selected_indices], *popt)
                lineData.setData(data[0, :], data[1, :])

                self.editingFinished[ax] = True

    def poly_fit(self):
        pass

    def smooth_points(self):

        dialog = QtWidgets.QDialog()
        dialog.setWindowIconText("Align value")
        dialog.setLayout(QtWidgets.QGridLayout())
        buttonBar = QtWidgets.QDialogButtonBox(QtWidgets.QDialogButtonBox.Ok | QtWidgets.QDialogButtonBox.Cancel)

        spinBoxPolyOrder = QtWidgets.QSpinBox()
        spinBoxWindowLength = QtWidgets.QSpinBox()
        spinBoxPolyOrder.setValue(2)
        spinBoxWindowLength.setValue(5)
        dialog.layout().addWidget(QtWidgets.QLabel("Window Length"), 0, 0)
        dialog.layout().addWidget(QtWidgets.QLabel("Polynomial Order"), 0, 1)
        dialog.layout().addWidget(spinBoxWindowLength, 1, 0)
        dialog.layout().addWidget(spinBoxPolyOrder, 1, 1)
        dialog.layout().addWidget(buttonBar, 2, 0, 1, 2)

        def accept():
            for ax in self.axes:
                print(self.editingFinished[ax])
                if not self.editingFinished[ax]:
                    lineData = ax.listDataItems()[0]
                    data = np.array(lineData.scatter.getData())
                    poly_order = spinBoxPolyOrder.value()
                    window_length = spinBoxWindowLength.value() if spinBoxWindowLength.value() % 2 == 1 else spinBoxWindowLength.value() + 1
                    try:
                        data[1, self.selected_indices] = savgol_filter(data[1, self.selected_indices],
                                                                       window_length=window_length,
                                                                       polyorder=poly_order)
                    except ValueError as err:
                        print("WARNING: " + err + ". Trim not done.")
                    lineData.setData(data[0, :], data[1, :])

                    self.editingFinished[ax] = True
            dialog.close()

        def reject():
            dialog.close()

        buttonBar.accepted.connect(accept)
        buttonBar.rejected.connect(reject)

        dialog.exec_()

    # CUSTOMIZED EVENTS
    # =================
    def emptyEvent(self, ev: QtGui.QMouseEvent):
        pass

    def scatterMouseDragEvent(self, ev: QtWidgets.QGraphicsSceneMouseEvent, axes: pg.PlotItem):
        # TODO: could probably be simplified
        lineData: pg.PlotDataItem = axes.listDataItems()[0]
        scatterData: pg.ScatterPlotItem = lineData.scatter

        if ev.button() != QtCore.Qt.LeftButton:
            ev.ignore()
            return

        if ev.isStart():
            # self.editingFinished[axes] = False
            self.localData = np.array(scatterData.getData())
            try:
                self.dragPoint = scatterData.pointsAt(ev.buttonDownPos())[0]
                self.dragOffset = -1 * ev.buttonDownPos()

                ev.accept()
                return
            except IndexError as err:
                # No point was in selection
                return

        elif ev.isFinish():
            data = np.array(lineData.getData())
            lineData.setData(data[0, :], data[1, :])
            self.editingFinished[axes] = True

        else:
            if self.dragPoint is None:
                ev.ignore()
                return
            else:
                # We are dragging a point. Find the local position of the event.
                data = np.copy(self.localData)
                data[1, self.selected_indices] += ev.pos().y() + self.dragOffset.y()

                lineData.setData(data[0, :], data[1, :])
                lineData.scatter.setBrush(self.brushList)

                return
        # pass

    def customMouseDragEvent(self, ev: QtGui.QMouseEvent, axes: pg.PlotItem):

        lineData: pg.PlotDataItem = axes.listDataItems()[0]

        ev.accept()
        pos = ev.pos()
        lastPos = ev.lastPos()
        dif = -1 * (pos - lastPos)

        if ev.button() & (QtCore.Qt.LeftButton | QtCore.Qt.MidButton):
            # if self.state['mouseMode'] == pg.ViewBox.RectMode:
            if ev.isFinish():
                axes.getViewBox().rbScaleBox.hide()
                ax: QtCore.QRectF = QtCore.QRectF(pg.Point(ev.buttonDownPos(ev.button())), pg.Point(pos))
                ax = axes.getViewBox().childGroup.mapRectFromParent(ax)

                # Mark markers in box and set selection - core process here
                # =========================================================
                x0, x1 = ax.left(), ax.right()
                y0, y1 = ax.top(), ax.bottom()

                data = np.array(lineData.getData())

                selection_x = np.logical_and(x0 < data[0, :], data[0, :] < x1)
                selection_y = np.logical_and(y0 < data[1, :], data[1, :] < y1)
                self.selected_indices = np.logical_and(selection_x, selection_y)

                self.selectedColor = QtGui.QColor('green')
                self.color = lineData.opts['symbolBrush'].color()
                self.penList = [pg.mkPen(color=self.selectedColor) if s else pg.mkPen(color=self.color) for s in
                                self.selected_indices]
                self.brushList = [pg.mkBrush(color=self.selectedColor) if s else pg.mkBrush(color=self.color) for s in
                                  self.selected_indices]

                lineData.scatter.setData(data[0, :], data[1, :], brush=self.brushList)

                self.editingFinished[axes] = False

                return
            else:
                axes.getViewBox().updateScaleBox(ev.buttonDownPos(), ev.pos())

                return
        # pass

    # IMPLEMENT RUN FUNCTION TO LAUNCH VIEWER
    # =======================================
    def run(self):
        pass

# def run(self):
#
#     momentum = self.get_japc_parameter("rmi://virtual_sps/SPSBEAM/MOMENTUM")
#     voltage = self.get_japc_parameter("rmi://virtual_sps/TWC200/TotalVoltage#totalVoltage")
#     area = self.get_japc_parameter("rmi://virtual_sps/SPSBEAM/MOMENTUM")
#
#     self.lines[0].setData(momentum[0, :-2], momentum[1, :-2])
#     self.lines[1].setData(voltage[0, :-2], voltage[1, :-2])
#
#     # axes: pg.PlotItem = self.axes[0]
#     # viewBox: pg.ViewBox = axes.getViewBox()
#     #
#     # pen: QtGui.QPen = dataItem.opts['pen']
#
#     scatterData = self.lines[0].scatter
#     scatterData.sigClicked.connect(lambda x: print("Data points: ", x))
