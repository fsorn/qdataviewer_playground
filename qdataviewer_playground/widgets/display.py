import numpy as np
from qtpy import QtWidgets

from qdataviewer_playground.widgets import QDataViewer


class Display(QDataViewer):

    def __init__(self, japc_cycle, window, parent=None, rows=3, cols=1, titles=[]) -> None:
        super().__init__(parent, rows, cols, titles)
        dataViewer: QtWidgets.QFrame = window.ui.qDataViewer
        dataViewer.setLayout(QtWidgets.QVBoxLayout())
        for i in reversed(range(dataViewer.layout().count())):
            dataViewer.layout().itemAt(i).widget().setParent(None)
        dataViewer.layout().addWidget(self)

    def get_japc_parameter(self, name: str):
        try:
            array = np.array(self.japc.getParam(name))
            ix = ~np.isnan(array[1, :])
            array = array[:, ix]
        except Exception as err:
            print(err)
            n_points = 200
            array = np.array([
                np.arange(n_points),
                np.random.rand(n_points)])

        return array

    def run(self):
        # momentum = self.get_japc_parameter("rmi://virtual_sps/SPSBEAM/MOMENTUM")
        # voltage = self.get_japc_parameter("rmi://virtual_sps/TWC200/TotalVoltage#totalVoltage")
        # area = self.get_japc_parameter("rmi://virtual_sps/SPSBEAM/MOMENTUM")

        self.lines[0].setData(momentum[0, :-2], momentum[1, :-2])
        self.lines[1].setData(voltage[0, :-2], voltage[1, :-2])

        # axes: pg.PlotItem = self.axes[0]
        # viewBox: pg.ViewBox = axes.getViewBox()
        #
        # pen: QtGui.QPen = dataItem.opts['pen']

        scatterData = self.lines[0].scatter
        scatterData.sigClicked.connect(lambda x: print("Data points: ", x))
