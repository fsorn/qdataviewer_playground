import sys

import numpy as np
from qtpy import QtWidgets
from qdataviewer_playground.widgets import QDataViewer


class MainWindow(QtWidgets.QMainWindow):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.qtdv = QDataViewer(rows=2)
        x = np.linspace(0, 6.0, 50.0)
        y = np.sin(x)
        for line in self.qtdv.lines:
            line.setData(x=x, y=y)
        self.show()
        self.resize(800, 600)
        self.setCentralWidget(self.qtdv)


def main():
    app = QtWidgets.QApplication(sys.argv)
    _ = MainWindow()
    sys.exit(app.exec_())


if __name__ == "__main__":
    main()
