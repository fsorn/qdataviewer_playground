"""
High-level tests for the  package.

"""

import qdataviewer_playground


def test_version():
    assert qdataviewer_playground.__version__ is not None
